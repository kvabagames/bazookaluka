﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExplosionController : MonoBehaviour {

	private AudioSource audioSource;

	public void PlayExplosionSound() {
		GetComponent<AudioSource>().Play();
	}
}
