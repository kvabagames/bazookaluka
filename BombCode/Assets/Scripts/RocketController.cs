﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RocketController : MonoBehaviour {

	private Rigidbody2D _rb;
	private Vector2 _startPos, _endPos;
	private AudioSource _audioSource;

	public int ParentPlayerId;

	[SerializeField]
	private GameObject[] Fumes;

	[SerializeField]
	private GameObject explosionPrefab;

	// Use this for initialization
	void Start () {
		_rb = GetComponent<Rigidbody2D>();
		_audioSource = GetComponent<AudioSource>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ShootMe (Vector2 rocketForce, int playerN) {

		ParentPlayerId = playerN;

		_startPos = transform.position;

		// get new gravity scale 
		float myGravityScale = 1;
		if (Mathf.Abs(rocketForce.x) > Mathf.Abs(rocketForce.y)) {
			myGravityScale = 1 - Mathf.Abs(rocketForce.x);
		} else {
			myGravityScale = 1 - Mathf.Abs(rocketForce.y);
		}

		// get new force
		Vector2 myForce = rocketForce * GLOBALS.instance.RocketSpeed;

		GetComponent<Rigidbody2D>().gravityScale = myGravityScale;
		GetComponent<Rigidbody2D>().AddForce(myForce, ForceMode2D.Impulse);

		Invoke("GenerateFume", .06f);
	}

	void OnCollisionEnter2D(Collision2D col)
    {
		_audioSource.Play();
		_endPos = transform.position;

		GameObject other = col.gameObject;

		if (other.tag == "Player") {
			other.GetComponent<PlayerControllerV4>().GetHit(ParentPlayerId, _startPos, _endPos);
			GameObject explosion = Instantiate(explosionPrefab, other.transform.position, Quaternion.identity);
			explosion.transform.localScale = new Vector3(1.2f,1.2f,1.2f);
			explosion.GetComponent<ExplosionController>().PlayExplosionSound();
		} else if (other.tag == "Rocket") {
			GameObject explosion = Instantiate(explosionPrefab, transform.position, Quaternion.identity);
			explosion.transform.localScale = new Vector3(.5f,.5f,.5f);
			explosion.GetComponent<ExplosionController>().PlayExplosionSound();
		} else {
			GameObject explosion = Instantiate(explosionPrefab, transform.position, Quaternion.identity);
			explosion.transform.localScale = new Vector3(.25f,.15f,.15f);
		}

		Destroy(gameObject);
    }

	void OnTriggerEnter2D(Collider2D col) {
		GameObject other = col.gameObject;

		if (col.gameObject.tag == "Bonus") {
			GameObject explosion = Instantiate(explosionPrefab, transform.position, Quaternion.identity);
			explosion.transform.localScale = new Vector3(.25f,.15f,.15f);
		}
		
		Destroy(gameObject);
    }

	void GenerateFume() {
		int rnd = Random.Range(0, Fumes.Length-1);
		GameObject newFume = Instantiate(Fumes[rnd], transform.position, Quaternion.identity);
		Invoke("GenerateFume", .06f);
	}
	
}
