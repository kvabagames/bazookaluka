﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeleportController : MonoBehaviour {

	public int TeleportID;

	public GameObject[] TeleportExits;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D col) {	
		Debug.Log("Trigger Detected");
		if (col.gameObject.tag == "Player") {
			int ExitId = Random.Range (0, TeleportExits.Length);
			ShowTeleportExit(ExitId);
			
			col.gameObject.transform.position = TeleportExits[ExitId].transform.position;

			GetComponent<AudioSource>().Play();
		}
    }

	void ShowTeleportExit(int teleportId) {
		TeleportExits[teleportId].GetComponent<SpriteRenderer>().enabled = true;

		StartCoroutine(HideTeleportExit(teleportId));		
	}

	IEnumerator HideTeleportExit (int teleportId)
    {        
        yield return new WaitForSeconds(1f);        
        TeleportExits[teleportId].GetComponent<SpriteRenderer>().enabled = false;
    }
}
