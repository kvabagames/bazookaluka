﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerControllerV4 : MonoBehaviour {

	public KeyCode InputLeft, InputRight, InputUp, InputDown;
	public float ForceMultiplierLeft, ForceMultiplierRight, ForceMultiplierUp, ForceMultiplierDown, Stamina, maxWalkSpeed, moveForce;
	public int PlayerNumber, PlayerScore;
	public delegate void GetHitDelegate(int playerID, int killerID, Vector2 rocketStartPos, Vector2 rocketEndPos);
	public event GetHitDelegate GetHitEvent;
	
	public GLOBALS.Bonuses? CurrentHeroBonus = null;

	private bool _isGrounded, _gunOverloaded, facingRight;
	private Rigidbody2D _rb;
	[SerializeField]
	private GameObject _rocketPrefab, _barTop, _heroBody, _shootAnim;
	[SerializeField]
	private SpriteRenderer _barBotSpriteRenderer;
	private Animator _bodyAnimator;


	void Awake () {
		// HARDCODED KEYBOARD CONROLS FOR WHEN YOU DO NOT HAVE A JOYSTICK CONNECTED
		if (Input.GetJoystickNames().Length < 1) {
			if (PlayerNumber == 1) {
				InputLeft = KeyCode.A;
				InputRight = KeyCode.D;
				InputUp = KeyCode.W;
				InputDown = KeyCode.S;
			} else if (PlayerNumber == 0) {
				InputLeft = KeyCode.LeftArrow;
				InputRight = KeyCode.RightArrow;
				InputUp = KeyCode.UpArrow;
				InputDown = KeyCode.DownArrow;
			}
		}
	}

	// Use this for initialization
	void Start () {
		_rb = GetComponent<Rigidbody2D>();
		_bodyAnimator = _heroBody.GetComponent<Animator>();	
		ForceMultiplierLeft = ForceMultiplierRight = ForceMultiplierUp = ForceMultiplierDown = GLOBALS.instance.StartForceMultiplier;	
		Stamina = GLOBALS.instance.MaxStamina;			
	}
	
	// Update is called once per frame
	void Update () {
		PlayerInput();
		
		PlayerTurn();
		IsGrounded();
		ClampHeroMaxSpeed();
		UpdateStamina();
		UpdateForceBar();
	}
	
	void FixedUpdate() {
		Walk();
	}

	private void PlayerInput() {
		
		if (_gunOverloaded == false) {		

			if (Input.GetKey(InputLeft) && Input.GetKey(InputRight) && Input.GetKey(InputUp) && Input.GetKey(InputDown)) {
				Debug.Log("ALL FOUR DIRECTIONS HAVE BEEN PUSHED");
				Stamina = 0;
				return;
			}	

			// Gain Force
			if (Input.GetKey(InputLeft) && ForceMultiplierLeft < GLOBALS.instance.MaxForceMultiplier) {
				ForceMultiplierLeft += GLOBALS.instance.ForceGainRate*Time.deltaTime;				
			}
			if (Input.GetKey(InputRight) && ForceMultiplierRight < GLOBALS.instance.MaxForceMultiplier) {
				ForceMultiplierRight += GLOBALS.instance.ForceGainRate*Time.deltaTime;
			}
			if (Input.GetKey(InputUp) && ForceMultiplierUp < GLOBALS.instance.MaxForceMultiplier) {
				ForceMultiplierUp += GLOBALS.instance.ForceGainRate*Time.deltaTime;
			}
			if (Input.GetKey(InputDown) && ForceMultiplierDown < GLOBALS.instance.MaxForceMultiplier) {
				ForceMultiplierDown += GLOBALS.instance.ForceGainRate*Time.deltaTime;
			}

			/******** SHOOT AND MOVE ********/		
			// SHOOT LEFT && MOVE RIGHT
			if (Input.GetKeyUp(InputLeft)) {
				Move(new Vector2(ForceMultiplierLeft * GLOBALS.instance.ForceLeftRight, 0));
				Shoot(new Vector2(GLOBALS.instance.RocketOffset * -1, 0), new Vector2(ForceMultiplierLeft * -1, 0), 0);
				ForceMultiplierLeft = GLOBALS.instance.StartForceMultiplier;

				Stamina -= GLOBALS.instance.StaminaPerShot;
			}

			// SHOOT RIGHT && MOVE LEFT
			if (Input.GetKeyUp(InputRight)) {
				Move(new Vector2(ForceMultiplierRight * -1 * GLOBALS.instance.ForceLeftRight, 0));
				Shoot(new Vector2(GLOBALS.instance.RocketOffset, 0), new Vector2(ForceMultiplierRight, 0), 180);
				ForceMultiplierRight = GLOBALS.instance.StartForceMultiplier;

				Stamina -= GLOBALS.instance.StaminaPerShot;
			}

			// SHOOT UP && MOVE DOWN
			if (Input.GetKeyUp(InputUp)) {
				Move(new Vector2(0, ForceMultiplierUp * -1 * GLOBALS.instance.ForceLeftRight));
				Shoot(new Vector2(0, GLOBALS.instance.RocketOffset), new Vector2(0, ForceMultiplierUp), 270);
				ForceMultiplierUp = GLOBALS.instance.StartForceMultiplier;

				Stamina -= GLOBALS.instance.StaminaPerShot;
				_bodyAnimator.SetTrigger("upTrigger");
			}

			// SHOOT DOWN && MOVE UP
			if (Input.GetKeyUp(InputDown)) {
				float doubleJumpMultiplier = 1;
				if (!_isGrounded) {
					doubleJumpMultiplier = GLOBALS.instance.DoubleJumpMultiplier;
				}
				Move(new Vector2(0, ForceMultiplierDown * GLOBALS.instance.ForceUp * doubleJumpMultiplier));
				Shoot(new Vector2(0, GLOBALS.instance.RocketOffset * -1f), new Vector2(0, ForceMultiplierDown * -1), 90);
				ForceMultiplierDown = GLOBALS.instance.StartForceMultiplier;

				Stamina -= GLOBALS.instance.StaminaPerShot;
				_bodyAnimator.SetTrigger("downTrigger");
			}
		}	

	}

	void Walk() {
		int JoystickNumber = PlayerNumber + 1;
		float h = Input.GetAxisRaw("Horizontal"+JoystickNumber);

		if (Mathf.Abs(h) > 0 && (Mathf.Abs(h * _rb.velocity.x) < maxWalkSpeed)) {
			_rb.AddForce(Vector2.right * h * moveForce); // horizontal movement
			if (!_bodyAnimator.GetBool("walk")) {
				Debug.Log("======= WALK");
				_bodyAnimator.SetBool("walk", true);
			}
		} 

		if (h == 0) {
			_bodyAnimator.SetBool("walk", false);
		}
	}

	private void PlayerTurn() {		
		
		if (Input.GetKey(InputLeft)) {
			if (transform.localScale.x > 0) {
				Flip();
			}
		} else if (Input.GetKey(InputRight)) {
			if (transform.localScale.x < 0) {
				Flip();
			}
		} else {

			int JoystickNumber = PlayerNumber + 1;
			float h = Input.GetAxisRaw("Horizontal"+JoystickNumber);

			if (h > 0 && transform.localScale.x < 0) {
				Flip();
			} else if (h < 0 && transform.localScale.x > 0) {
				Flip();
			}			
		}
		
	}

	private void Flip() {
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
		facingRight = !facingRight;
	}

	private void IsGrounded() {
		RaycastHit2D[] hits = Physics2D.CircleCastAll(transform.position, .25f, Vector2.down, .25f, LayerMask.GetMask("Ground"));
		if (hits.Length > 0) {
			_isGrounded = true;	
		} else {
			_isGrounded = false;
			if (_bodyAnimator.GetBool("walk")) {
				_bodyAnimator.SetBool("walk", false);
			}
		}
		// Debug.Log(_isGrounded);
	}

	private void Move(Vector2 force) {

		if (Mathf.Abs(force.x) > GLOBALS.instance.MaxForceMultiplier * .8) {
			if (_bodyAnimator.GetBool("pushback") == false) {
				_bodyAnimator.SetTrigger("pushbackTrigger");
			}
		}
		
		if (force.y > 0 && _rb.velocity.y < 0 && !_isGrounded) { //if double jump kill inertia
			_rb.velocity = new Vector2(_rb.velocity.x, 0 );
		}
		_rb.AddForce(force,ForceMode2D.Impulse);
		
	}

	private void Shoot(Vector3 offset, Vector2 rocketForce, float rotation) {
		// offset
		Vector3 _rocketOffset = transform.position + offset;

		// instantiate
		GameObject _newRocket = Instantiate(_rocketPrefab, _rocketOffset, Quaternion.identity);
		_newRocket.transform.Rotate(_newRocket.transform.rotation.x, _newRocket.transform.rotation.y, rotation);

		GameObject _newShootAnim = Instantiate(_shootAnim, _rocketOffset, Quaternion.identity);
		_newShootAnim.transform.Rotate(_shootAnim.transform.rotation.x, _shootAnim.transform.rotation.y, rotation-180);

		// shoot
		_newRocket.GetComponent<RocketController>().ShootMe(rocketForce, PlayerNumber);
	}

	private void ClampHeroMaxSpeed() {
		if (Mathf.Abs(_rb.velocity.x) > 5) {
			_rb.velocity = new Vector2(Mathf.Clamp(_rb.velocity.x, -5, 5),Mathf.Clamp(_rb.velocity.y, -5, 5));
		}
	}

	public void GetHit(int killerID, Vector2 rocketStartPos, Vector2 rocketEndPos) {

		if (CurrentHeroBonus == GLOBALS.Bonuses.SHIELD) {
			return;
		}
		if (GetHitEvent != null) {
			Debug.Log("called the delegate by " + PlayerNumber);
			GetHitEvent(killerID, PlayerNumber, rocketStartPos, rocketEndPos);
		}
	}

	public void Revive() {
		gameObject.SetActive(true);
	}

	private void UpdateStamina() {
		
		if (Stamina < GLOBALS.instance.StaminaPerShot && _gunOverloaded == false) {
			_gunOverloaded = true;
			_barBotSpriteRenderer.color = Color.red;
			Invoke("SetGunOverloadFalse", 2);
		} 

		if (Stamina < GLOBALS.instance.MaxStamina) {
			Stamina += GLOBALS.instance.StaminaGainRate * Time.deltaTime;
		} else {
			Stamina = GLOBALS.instance.MaxStamina;
		}
	}

	private void SetGunOverloadFalse() {
		_barBotSpriteRenderer.color = Color.white;
		_gunOverloaded = false;
		Stamina = GLOBALS.instance.MaxStamina;
	}

	private void UpdateForceBar() {
		float staminaAmount = 1 - (Stamina / GLOBALS.instance.MaxStamina);
		_barTop.transform.localScale = new Vector2(staminaAmount,1);
	}

	public void EnableHeroBonus(GLOBALS.Bonuses bonus) {
		CurrentHeroBonus = bonus;
		Debug.Log("NEW BONUS: " + CurrentHeroBonus);

		// SHIELD, INVISIBILITY, STAMINA, BFG, BIG, SMALL, WEAK
		switch (CurrentHeroBonus) {
			case GLOBALS.Bonuses.SHIELD:

				break;
			case GLOBALS.Bonuses.INVISIBILITY:

				break;
			case GLOBALS.Bonuses.STAMINA:

				break;
			case GLOBALS.Bonuses.WEAK:

				break;
			default:
				break;			
		}

		Invoke("ResetHeroBonuses",10f);
	}
	
	private void ResetHeroBonuses() {
		CurrentHeroBonus = null;
	}
}
