﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BonusController : MonoBehaviour {		

	[SerializeField] private float bonusChangeTime = 3f;
	private GLOBALS.Bonuses CurrentBonusType;

	void Start () {
		CurrentBonusType = (GLOBALS.Bonuses)Random.Range(0, GLOBALS.Bonuses.GetNames(typeof(GLOBALS.Bonuses)).Length);
		Invoke("ChangeBonusType", bonusChangeTime);
	}
	
	void ChangeBonusType() {
		GLOBALS.Bonuses newBonusType;
		newBonusType = (GLOBALS.Bonuses)Random.Range(0, GLOBALS.Bonuses.GetNames(typeof(GLOBALS.Bonuses)).Length);
		if (newBonusType != CurrentBonusType) {
			CurrentBonusType = newBonusType;
			Invoke("ChangeBonusType", bonusChangeTime);
		} else {
			ChangeBonusType();
		}
		
	}

	void OnTriggerEnter2D(Collider2D col) {
		GameObject other = col.gameObject;
		if (col.gameObject.tag == "Player") {
			other.GetComponent<PlayerControllerV4>().EnableHeroBonus(CurrentBonusType);
		}		
		Destroy(gameObject);
    }
}
