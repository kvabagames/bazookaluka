﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BigMessageController : MonoBehaviour {

	private float timeOfLastTextChange;
	private Text BigText;

	// Use this for initialization
	void Start () {
		BigText = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
		ClearBigText();
	}

	public void ChangeBigText(string newText) {
		timeOfLastTextChange = Time.time;
		BigText.text = newText;
	}

	private void ClearBigText() {
		if (timeOfLastTextChange < Time.time - 1f) {
			BigText.text = "";
		}
	}
}
