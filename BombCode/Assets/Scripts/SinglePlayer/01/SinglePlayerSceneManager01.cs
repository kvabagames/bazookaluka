﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SinglePlayerSceneManager01 : MonoBehaviour {

	[SerializeField]
	private GameObject PlayerPrefab;

	private GameObject Player;

	// Use this for initialization
	void Start () {
		Player = Instantiate(PlayerPrefab);
		Player.GetComponent<Rigidbody2D>().MovePosition(new Vector2());
		Player.GetComponent<PlayerControllerV4>().PlayerNumber = 0;
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.P) || Input.GetKey("joystick button 7")) {
			SceneManager.LoadScene("MainMenu");
		}		
	}

	
}
