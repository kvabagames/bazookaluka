﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossManager01 : MonoBehaviour {

	[SerializeField]
	private GameObject ladder, tank, cannon, head, rocket;

	[SerializeField]
	private float minLadderSpeedX, maxLadderSpeedX, minLadderPosX, maxLadderPosX, minLadderWait, maxLadderWait,
	minTankSpeedY, maxTankSpeedY, minTankPosY, maxTankPosY, minTankWait, maxTankWait;

	private float nextLadderPosX, nextTankPosY, currentTime, timeToMove;

	private enum bossStateEnum {SEEK, SHOOT, LAUGH, RAGE};
	private bossStateEnum bossState;

	// Use this for initialization
	void Start () {
		// bossState = bossStateEnum.SEEK;
		SetNextBossCoordinates();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void FixedUpdate () {

		switch (bossState) {
			case bossStateEnum.SEEK:
				StateSEEK();
				break;
			case bossStateEnum.SHOOT:
				StateSHOOT();
				break;
			case bossStateEnum.LAUGH:
				StateLAUGH();
				break;
			case bossStateEnum.RAGE:
				StateRAGE();
				break;
			default:
				Debug.LogError("WRONG bossState value");
				break;
		}
	}

	void SetNextBossCoordinates() {
		nextLadderPosX = Random.Range(minLadderPosX, maxLadderPosX);
		nextTankPosY = Random.Range(minTankPosY, maxTankPosY);
		currentTime = 0f;
		timeToMove = Random.Range(1f, 3f);
		Debug.Log("nextLadderPosX = " + nextLadderPosX);
		Debug.Log("nextTankPosY = " + nextTankPosY);

		bossState = bossStateEnum.SEEK;
		// return new Vector2(nextLadderPosX, nextTankPosY);
	}

	void StateSEEK () {
		Debug.Log("SEEK");
		if (ladder.transform.position.x == nextLadderPosX) {
			bossState = bossStateEnum.SHOOT;
			Invoke("SetNextBossCoordinates",1.5f); // FIX ME
			//bossState = bossStateEnum.SEEK;
		} else {
			currentTime += Time.deltaTime;
			ladder.transform.position = Vector2.Lerp(ladder.transform.position, new Vector2(nextLadderPosX, ladder.transform.position.y), currentTime / timeToMove);
			tank.transform.position = Vector2.Lerp(tank.transform.position, new Vector2(tank.transform.position.x, nextTankPosY), currentTime / timeToMove);
		}
		
	}

	void StateSHOOT () {
		Shoot(new Vector2(Random.Range(.4f,1f), 0f), 45f);
		bossState = bossStateEnum.LAUGH;
	}

	void StateLAUGH () {
		Debug.Log("LAUGH");
		
	}

	void StateRAGE () {

	}


	private void Shoot(Vector2 rocketForce, float rotation) {
		// offset
		Vector3 _rocketOffset = tank.transform.position + new Vector3(1.2f, 0, 0);

		// instantiate
		GameObject _newRocket = Instantiate(rocket, _rocketOffset, Quaternion.identity);
		_newRocket.transform.Rotate(_newRocket.transform.rotation.x, _newRocket.transform.rotation.y, rotation);

		// GameObject _newShootAnim = Instantiate(_shootAnim, _rocketOffset, Quaternion.identity);
		// _newShootAnim.transform.Rotate(_shootAnim.transform.rotation.x, _shootAnim.transform.rotation.y, rotation-180);

		// shoot
		_newRocket.GetComponent<RocketController>().ShootMe(rocketForce, 1);
	}

	
}
