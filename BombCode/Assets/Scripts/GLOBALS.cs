﻿using System.Collections;
using UnityEngine;

public class GLOBALS : MonoBehaviour {
	public static GLOBALS instance = null;

	public float 
	ForceLeftRight = 10f, 
	ForceUp = 10f, 
	MoveForce = 10f,
	DoubleJumpMultiplier = .3f,
	RocketOffset = .5f,
	RocketSpeed = 20f, 
	ForceGainRate = .8f,
	StartForceMultiplier = .25f,
	MaxForceMultiplier = 1f,
	MaxHeroSpeed = 5f,
	MaxScore = 10,
	MaxStamina = 10,
	StaminaGainRate = 4,
	StaminaPerShot = 2;

	public enum Bonuses
	{
		SHIELD, INVISIBILITY, STAMINA, BFG, BIG, SMALL, WEAK
	}

	void Awake () {
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			Debug.Log("GLOBALS EXISTED - DESTROYING GAMEOBJECT");
			Destroy(gameObject);
		}
	}
}