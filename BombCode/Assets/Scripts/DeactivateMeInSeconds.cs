﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeactivateMeInSeconds : MonoBehaviour {

	public void StartDeactivatingMeInSeconds(float seconds) {
		Invoke ("DeactivateMe", seconds);
	}

	private void DeactivateMe () {
		gameObject.SetActive(false);
	}
}
