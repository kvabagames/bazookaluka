﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyOnAnimationEnd : MonoBehaviour {

	[SerializeField]
	float delay;

	void Start() {
		Destroy (gameObject, this.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).length + delay);
	}

	/*
	public void DieInSeconds() {
		Invoke("Die", delay);
	}

	void Die() {
		Destroy(gameObject);
	}
	 */
	
}
