﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour {

	[SerializeField]
	private Text ScoreText, WinText;

	[SerializeField]
	private GameObject ReplayButton;

	// PLAYERS
	[SerializeField]
	private GameObject[] PlayersPrefabs;
	public List<GameObject> PlayersList;

	[SerializeField]
	int NumberOfPlayers;

	private GameObject BigMessage;

	// Use this for initialization
	void Start () {

		BigMessage = GameObject.FindGameObjectWithTag("BigMessage");

		ReplayButton.SetActive(false);

		PlayersList = new List<GameObject>();
		
		for (int i=0; i < NumberOfPlayers; i++) {
			SpawnNewPlayer(i, new Vector3());
			PlayersList[i].GetComponent<PlayerControllerV4>().GetHitEvent += UpdateScore;
		}

		// for (int i = 0; i < Input.GetJoystickNames().Length; i++) {
		// 	Debug.Log(Input.GetJoystickNames()[i]);
		// }
		
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.P) || Input.GetKey("joystick button 7")) {
			SceneManager.LoadScene("MainMenu");
		}

		// if (Input.GetAxis("Horizontal") != 0 || Input.GetAxis("Vertical") != 0) {
		// 	Debug.Log(Input.GetAxis("Horizontal") + ", " + Input.GetAxis("Vertical"));
		// }

		// if (Input.GetAxis("JRS X") != 0 || Input.GetAxis("JRS Y") != 0) {
		// 	Debug.Log("JRS: " + Input.GetAxisRaw("JRS X") + ", " + Input.GetAxisRaw("JRS Y"));
		// }

		//  for (int i = 0;i < 20; i++) {
        //      if(Input.GetKeyDown("joystick button "+i)){
        //          print("joystick button "+i);
        //      }
        //  }
	}

	public void UpdateScore(int killerID, int killedID, Vector2 rocketStartPos, Vector2 rocketEndPos) {
		int scoreChange = 1;

		PlayersList[killedID].gameObject.SetActive(false);

		if (killerID == killedID) {
			Debug.Log("SUICIDE");
			scoreChange = -1;
			BigMessage.GetComponent<BigMessageController>().ChangeBigText("SUICIDE... LOL...");
		} else {
			Debug.Log(killerID + " KILLED " + killedID);
			if (rocketStartPos.y > rocketEndPos.y && Mathf.Abs(rocketStartPos.x - rocketEndPos.x) < .25f) {
				BigMessage.GetComponent<BigMessageController>().ChangeBigText("DEATH FROM ABOVE!!!");
			} else if (Mathf.Abs(rocketStartPos.x - rocketEndPos.x) > 4) {
				BigMessage.GetComponent<BigMessageController>().ChangeBigText("LONG SHOT");
			} if (rocketStartPos.y < rocketEndPos.y && Mathf.Abs(rocketStartPos.x - rocketEndPos.x) < .25f) {
				BigMessage.GetComponent<BigMessageController>().ChangeBigText("DEATH FROM BELOW!!!");
			} 
		}		
		
		PlayersList[killerID].GetComponent<PlayerControllerV4>().PlayerScore += scoreChange;
		
		ScoreText.text = PlayersList[1].GetComponent<PlayerControllerV4>().PlayerScore + " : " + PlayersList[0].GetComponent<PlayerControllerV4>().PlayerScore;
		if (PlayersList[killerID].GetComponent<PlayerControllerV4>().PlayerScore >= GLOBALS.instance.MaxScore) {
			Win(killerID);
		} else {
			StartCoroutine(RevivePlayer(PlayersList[killedID]));
		}
		 
	}

	IEnumerator RevivePlayer (GameObject DeadPlayer)
	{
		yield return new WaitForSeconds (1.5f); 		
		DeadPlayer.transform.position = new Vector2(Random.Range(-8f,8f),Random.Range(-4f,1f));
		DeadPlayer.GetComponent<PlayerControllerV4>().Stamina = GLOBALS.instance.MaxStamina;
		DeadPlayer.SetActive(true);
	}

	private void SpawnNewPlayer(int PlayerId, Vector2 PlayerPos) {
		PlayersList.Add(Instantiate(PlayersPrefabs[PlayerId]));
		PlayersList[PlayersList.Count - 1].GetComponent<PlayerControllerV4>().PlayerNumber = PlayerId;
		PlayersList[PlayersList.Count - 1].GetComponent<PlayerControllerV4>().PlayerScore = 0;
	}

	private void Win (int WinnerID) {
		WinText.text = "Player " + WinnerID + " WINS!";
		ReplayButton.SetActive(true);
	}
	
	public void PlayAgain() {
		WinText.text = "";
		SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		ReplayButton.SetActive(false);
	}

}
